﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
  public GameObject predatorObject;
  public Transform predatorSpawnPoint;

  private void Start()
  {
    Instantiate(predatorObject, predatorSpawnPoint.position, predatorSpawnPoint.rotation, predatorSpawnPoint);
  }
}
