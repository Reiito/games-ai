﻿using UnityEngine;

public enum IN { PATH, OPEN, CLOSED, NONE } ;

public class Node : IHeapItem<Node>
{
  public bool walkable; //state
  public Vector3 worldPosition; //where is node
  public int gridX;
  public int gridY;
  public int movementPenalty;
  public IN inList;

  public int gCost; //distance from starting node
  public int hCost; //distance from end node
  public int FCost { get { return gCost + hCost; } } //cost of node

  public Node parent;

  // position in heap
  int heapIndex;
  public int HeapIndex
  {
    get { return heapIndex; }
    set { heapIndex = value; }
  }

  // class constructor
  public Node(bool walkable, Vector3 worldPosition, int gridX, int gridY, int movementPenalty, IN inList)
  {
    this.walkable = walkable;
    this.worldPosition = worldPosition;
    this.gridX = gridX;
    this.gridY = gridY;
    this.movementPenalty = movementPenalty;
    this.inList = inList;
  }

  // compare two node's f costs
  public int CompareTo(Node nodeToCompare)
  {
    int compare = FCost.CompareTo(nodeToCompare.FCost);
    if (compare == 0) //if f costs the same
      compare = hCost.CompareTo(nodeToCompare.hCost); //compare h costs

    return -compare; //return lowest one (negate)
  }
}
