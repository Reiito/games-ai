﻿using System;
using System.Collections.Generic;
using UnityEngine;

// NOTE: explanation in "../../Information/algorithm.txt"
public class PathFinding : MonoBehaviour
{
  LevelGrid grid;

  const int DIAGONAL_DISTANCE = 14;
  const int HORIZONAL_VERTICAL_DISTANCE = 10;


  // MonoBehaviour functions

  private void Awake()
  {
    // set grid reference
    grid = GetComponent<LevelGrid>();
  }


  // Non-Mono functions
  public void FindPath(PathRequest request, Action<PathResult> callback)
  {
    Vector3[] waypoints = new Vector3[0]; //store points into array
    bool pathSuccess = false;

    // set up starting point and target point
    Node startNode = grid.NodeFromWorldPoint(request.pathStart);
    Node endNode = grid.NodeFromWorldPoint(request.pathEnd);
    startNode.parent = startNode;

    if (startNode.walkable && endNode.walkable) //check if path is valid
    {
      Heap<Node> openSet = new Heap<Node>(grid.MaxSize); //the set of values to be evaluated
      HashSet<Node> closedSet = new HashSet<Node>(); //the set of nodes already evaluated
      openSet.Add(startNode); //add first node to open
      startNode.inList = IN.OPEN;

      while (openSet.Count > 0) //loop while set isn't empty
      {
        // remove node from heap(open) and add to hashset(closed)
        Node currentNode = openSet.RemoveFirst();
        closedSet.Add(currentNode);
        currentNode.inList = IN.CLOSED;

        // exit condition (end found)
        if (currentNode == endNode)
        {
          pathSuccess = true;
          break;
        }

        // loop through current nodes neighbours
        foreach (Node neighbour in grid.GetNeighbours(currentNode))
        {
          if (!neighbour.walkable || closedSet.Contains(neighbour)) //skip unwalkable & closed neighbours
            continue;

          // get cost to neighbours
          int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.movementPenalty;
          if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour)) //better path
          {
            neighbour.gCost = newMovementCostToNeighbour; //new g cost
            neighbour.hCost = GetDistance(neighbour, endNode); //new distance to end
            neighbour.parent = currentNode; //make current node a parent

            if (!openSet.Contains(neighbour)) //add to check set
            {
              openSet.Add(neighbour);
              neighbour.inList = IN.OPEN;
            }
            else //update it's details
              openSet.UpdateItem(neighbour);
          }
        }
      }
    }

    // set final path on success
    if (pathSuccess)
    {
      waypoints = RetracePath(startNode, endNode);
      pathSuccess = waypoints.Length > 0;
    }

    callback(new PathResult(waypoints, pathSuccess, request.callback));
  }

  // get the distance from a node to another
  int GetDistance(Node nodeA, Node nodeB)
  {
    // get displacement values
    int distanceX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
    int distanceY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

    if (distanceX > distanceY) //more horizontal
      return DIAGONAL_DISTANCE * distanceY + HORIZONAL_VERTICAL_DISTANCE * (distanceX - distanceY);
    else //more vertical
      return DIAGONAL_DISTANCE * distanceX + HORIZONAL_VERTICAL_DISTANCE * (distanceY - distanceX);
  }

  // retrace and complete the path
  Vector3[] RetracePath(Node startNode, Node endNode)
  {
    List<Node> path = new List<Node>();
    Node currentNode = endNode; //start from the end

    while (currentNode != startNode) //loop until back to start node
    {
      // add to list and go to the next node
      path.Add(currentNode);
      currentNode.inList = IN.PATH;
      currentNode = currentNode.parent;
    }

    Vector3[] waypoints = SimplifyPath(path); //create simpler waypoints
    Array.Reverse(waypoints); //switch order from end->start to start->end

    return waypoints;
  }

  // place waypoints only when the path changes direction
  Vector3[] SimplifyPath(List<Node> path)
  {
    List<Vector3> waypoints = new List<Vector3>();
    Vector2 directionOld = Vector2.zero;

    for (int i = 1; i < path.Count; i++)
    {
      // find direction between old & new points
      Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
      if (directionNew != directionOld) //if direction is different, add to list
        waypoints.Add(path[i].worldPosition);
      directionOld = directionNew;
    }

    return waypoints.ToArray();
  }
}
