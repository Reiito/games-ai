﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
  public GameObject player;
  public float cameraDistance = 6.0f;

  void LateUpdate()
  {
    transform.position = player.transform.position - player.transform.forward * cameraDistance;
    transform.LookAt(player.transform.position);
    transform.position = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);
  }
}
