﻿using System;

// genric heap class implementation
// NOTE: explanation in "../../Information/heap.txt"
public class Heap<T> where T : IHeapItem<T>
{
  T[] items; //content array
  int currentItemCount; //heap count
  public int Count { get { return currentItemCount; } }

  // construct heap (size)
  public Heap(int maxHeapSize)
  {
    items = new T[maxHeapSize];
  }

  // add item to heap
  public void Add(T item)
  {
    item.HeapIndex = currentItemCount; //set index
    items[currentItemCount] = item; //add to end of array
    SortUp(item); //sort item up the array
    currentItemCount++;
  }

  // remove lowest value in heap (first)
  public T RemoveFirst()
  {
    // remove first
    T firstItem = items[0];
    currentItemCount--;
    // set last to first
    items[0] = items[currentItemCount];
    items[0].HeapIndex = 0;
    // sort to correct order
    SortDown(items[0]);

    return firstItem;
  }

  // update item's order
  public void UpdateItem(T item)
  {
    SortUp(item);
  }

  // check if item is in array
  public bool Contains(T item)
  {
    return Equals(items[item.HeapIndex], item);
  }


  // move item up array
  void SortUp(T item)
  {
    int parentIndex = (item.HeapIndex - 1) / 2; //get parent ((n-1)/2)

    // loop until item is in correct place
    while (true)
    {
      T parentItem = items[parentIndex];
      if (item.CompareTo(parentItem) > 0)
        Swap(item, parentItem);
      else
        break;
    }
  }

  // move item down array
  void SortDown(T item)
  {
    // loop until item is in correct place
    while (true)
    {
      int childIndexLeft = 2 * item.HeapIndex + 1; //left child (2n+1)
      int childIndexRight = 2 * item.HeapIndex + 2; //right child (2n+2)
      int swapIndex = 0;

      if (childIndexLeft < currentItemCount) //has child
      {
        swapIndex = childIndexLeft;
        if (childIndexRight < currentItemCount) //has second child
          if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0) //which is lowest value child
            swapIndex = childIndexRight;

        if (item.CompareTo(items[swapIndex]) < 0) //parent smaller than child, swap
          Swap(item, items[swapIndex]);
        else //exit when smallest
          return;
      }
      else //exit when no children
        return;
    }
  }

  // swap within array
  void Swap(T itemA, T itemB)
  {
    // swap items
    items[itemA.HeapIndex] = itemB;
    items[itemB.HeapIndex] = itemA;
    // swap heap indexes
    int itemAIndex = itemA.HeapIndex;
    itemA.HeapIndex = itemB.HeapIndex;
    itemB.HeapIndex = itemAIndex;
  }
}

// item interface
public interface IHeapItem<T> : IComparable<T>
{
  int HeapIndex { get; set; }
}
