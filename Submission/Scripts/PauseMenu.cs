﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
  public static bool isPaused = false;
  public static bool isEnd = false;
  public GameObject pauseMenuUI;

  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape))
    {
      if (!ScoreManager.end)
      {
        if (isPaused)
        {
          Resume();
        }
        else
        {
          Pause();
        }
      }
    }
  }

  public void Resume()
  {
    pauseMenuUI.SetActive(false);
    Time.timeScale = 1f;
    isPaused = false;
  }

  void Pause()
  {
    pauseMenuUI.SetActive(true);
    Time.timeScale = 0f;
    isPaused = true;
  }

  public void QuitGame()
  {
    Application.Quit();
  }

  public void LoadScene(string levelName)
  {
    Time.timeScale = 1f;
    SceneManager.LoadScene(levelName);
  }
}
