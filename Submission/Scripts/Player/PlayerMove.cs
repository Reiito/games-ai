﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

  public static Vector3 myPos;
  public float speed = 10.0f;

  void Update()
  {

    myPos = transform.position;

    Vector3 fwd = transform.TransformDirection(Vector3.forward);
    RaycastHit hit;
    if (Physics.Raycast(transform.position, fwd, out hit, 2.0f))
    {
      if (hit.transform.tag == "Obstacle")
        speed = -1.0f;
    }
    else
      speed = 10.0f;

    var x = Input.GetAxis("Horizontal") * Time.deltaTime * 300.0f;
    var z = Input.GetAxis("Vertical") * Time.deltaTime * speed;

    transform.Rotate(0, x, 0);
    transform.Translate(0, 0, z);
  }
}