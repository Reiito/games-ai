﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{

  protected delegate bool transitionCondition();
  protected delegate void state();
  protected List<transitionCondition> transitionConditionList = new List<transitionCondition>();
  protected List<state> stateList = new List<state>();
  protected state currentState;
  protected state defaultState;

  void Start()
  {

  }

  // Go trough each condition and if any return true set the current state to the corresponding method
  protected void Update()
  {

    currentState();

    bool foundCondition = false;
    for (int i = 0; i < transitionConditionList.Count; i++)
    {
      if (transitionConditionList[i]() == true)
      {
        currentState = stateList[i];
        foundCondition = true;
      }
    }
    if (!foundCondition)
      currentState = defaultState;
  }

  protected void SetDefaultstate(state begState)
  {
    currentState = begState;
    defaultState = begState;
  }

  protected void AddStateAndCondition(state newState, transitionCondition newCondition)
  {
    transitionConditionList.Add(newCondition);
    stateList.Add(newState);
  }

}
